#! /bin/bash

if [ -z "$SITE_URL" ]; then
	SITE_URL="https://www.sewatech.fr"
fi

if [ -z "$PDF_DIR" ]; then
	PDF_DIR=_site/pdf
fi

if [ ! -d $PDF_DIR ]; then
	mkdir -p $PDF_DIR
fi

for file in ./_courses/*; do
	if [ -f "$file" ]; then
		margin=15
		course_url=$(cat $file | grep permalink: | cut -d ' ' -f 2)
		if [ -z "$course_url" ]; then
			name="formation-$(basename $file .adoc)"
		else
			name=$(basename $course_url .html)
		fi
		echo $name
		wkhtmltopdf -L $margin -R $margin -T $margin -B 10 --header-html "$SITE_URL/pdf-logo.html" --header-line --footer-center "[webpage]" --footer-right "[page]/[topage]" --footer-line --print-media-type --background --quiet $SITE_URL/$name.html $PDF_DIR/$name.pdf \
			&& echo "Printed $SITE_URL/$name.html => $PDF_DIR/$name.pdf" || exit 1
	fi
done
