serve:
	bundle exec jekyll serve

draft:
	bundle exec jekyll serve --draft

print:
	SITE_URL=http://localhost:4000 PDF_DIR=pdf .ci/pdf.sh