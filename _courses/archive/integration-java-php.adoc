---
layout: course
permalink: formation-integration-java-php.html
title: Intégration Java / PHP
title-short: Intégration Java - PHP
publication: 2009-02-16
code: dj91
author: alexis
teachers: 

type: pratique
duration: 2
price-level: 3
target: Cette formation s'adresse aux architectes, concepteurs et développeurs Java ou PHP souhaitant faire cohabiter les deux langages.
practice: Les travaux pratiques sont réalisés avec PHP 5, Java 5 ou 6 et Tomcat.
track: java
logo: 
importance: 
mode: 
keywords: 
- formation
intro: <p>Quel est le meilleur langage de programmation du marché&nbsp;? Cette question n'a pas de réponse unique. Au lieu de chercher la solution universelle, les architectures orientées services nous permettent de choisir des solutions adaptées aux différents contextes. Il reste alors à intégrer l'ensemble grâce aux techniques d'interopérabilité.</p><p>Dans cette formation, vous verrez les différentes techniques d'interopérabilité en Java et PHP, vous étudierez la façon d'implémenter des WebServices entre ces deux langages, puis vous apprendrez comment mettre en place les techniques spécifiques que sont le Java / PHP Bridge et Quercus.</p>
---

Objectif de l'intégration

- Architectures Orientées Services
- Intégration de progiciel
- Stratégie &#171;&nbsp;Best of Bread&nbsp;&#187;
- Fusion de systèmes d'informations
- Application multi-langages
- Java Scripting API (JSR-223)

WebService&nbsp;: SOAP et REST

- Définition des WebServices
- Protocoles SOAP et WSDL
- Mise en place en PHP et Java
- Principes de REST
- Solutions en PHP et Java

Pont Java-PHP

- Présentation du Pont Java-PHP
- Pré-requis et Installation
- Intégration par CGI
- Implémentation de la Java Scripting API
- Appel de Java depuis PHP

Intégration in-process avec Quercus

- Principe de l'intégration in-process
- Présentation de Caucho, Resin et Quercus
- Mise en œuvre Quercus de Resin
- Installation de Quercus dans Tomcat
- Installation de Quercus dans les autres serveurs d'applications

Synthèse et comparatif

- Avantages et inconvénients de chaque solution
- Portabilité et respect des normes
- Performances
- Facilité de mise en œuvre
