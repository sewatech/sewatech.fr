---
layout: course
permalink: formation-jboss4.html
title: Administration JBoss 4
title-short: Formation JBoss
publication: 2005-09-22
code: AJ01
author: alexis
teachers: 

type: pratique
duration: 3
price-level: 3
target: <p>Administrateurs, intégrateurs et développeurs JavaEE ayant une connaissance générale de techniques JavaEE (Servlet, JSP, EJB,...)</p><p>Remarque&nbsp;&colon; le séminaire &#171;&nbsp;<a href="formation-synthese-javaee.html">JavaEE&nbsp;&colon; tour d'horizon</a>&nbsp;&#187; est un excellent moyen d'atteindre ces prérequis</p>
practice: <p>Pour chaque partie, mise en œuvre pratique avec déploiement d'applications.</p><p>Ils sont réalisés avec le JDK 5.0 de Sun et JBoss 4.0 ou 4.2, sous Windows, Linux ou Unix (Solaris,...).</p>
track: administration
logo: 
importance: 
mode: 
keywords: 
- formation
- jboss
intro: <p>JBoss est le leader des serveurs d'application JavaEE libres. Son implantation sur le marché est en constante progression.</p><p>Ce cours vous présentera les caractéristiques de JBoss. Il vous apprendra à installer et configurer JBoss, à déployer et optimiser des applications JavaEE dans JBoss. Il vous présentera aussi les principes d'administration avancée de JBoss (surveillance, optimisation,...).</p>
---

Introduction à JavaEE (rappels)

- Les principes fondamentaux de java et de JavaEE
- Les principaux composants de JavaEE (JSP, servlet, EJB, JMS,...)
- L'essentiel d'XML

Introduction à JBoss

- JBoss, le serveur d'application certifié JavaEE de RedHat
- Les principes de l'Open Source et les types de licences associées
- L'architecture de JBoss (kernel, JMX et AOP)

Les bases de l'administration JBoss

- L'installation de JBoss et les pré-requis
- Le principe des configurations
- Le déploiement d'applications JavaEE (ear), d'applications Web (war) et de composants EJB (jar)
- Le chargement de classes et son impact sur le déploiement des librairies

La gestion des configurations

- Les configurations standards (minimal, default, all) et configurations personnalisées
- L'architecture de déploiement
- Le déploiement de datasources et de connecteurs JCA (rar)
- Le déploiement de services (sar) et de MBeans (JMX)
- L'inspection du serveur avec JMX (console, twiddle et accès RMI)
- L'intégration avec Tomcat&nbsp;; le remplacement de Tomcat par Jetty

Le suivi du serveur

- La gestion des traces avec Log4J et Chainsaw
- L'inspection du serveur avec la console web, ses fonctions d'alertes et ses graphiques
- L'analyse des queues et sujets JMS avec Hermes
- Le suivi des requêtes SQL avec le proxy JDBC P6Spy et IronGrid
- Les outils de monitoring du JDK&nbsp;: jconsole, jps, jstat,...

Les fonctions avancées d'administration

- Les techniques d'invocation d'EJB (http, pooled,...)
- Affiner la chaîne d'intercepteurs
- La technique de script BeanShell
- Les notifications en MBeans
- La planification de tâches (scheduler et time service)

L'optimisation des performances

- Les réglages de la machine virtuelle (hotspot, Xmx, Xms,...)
- Affiner le dimensionnement des pools (EJB, DataSource, threads)
- Les autres optimisations classiques
- La mise en cluster pour la tolérance de panne (fail over) et la répartition de charge (load balancing)

La sécurité du serveur et des applications

- Les principes de sécurisation du serveur (réseau, security manager, applications critiques)
- Le modèle de sécurité JBoss (JBossSx)
- La gestion des autorisations et des authentifications en JavaEE (JAAS)
- La sécurisation des échanges avec SSL

Quelques compléments

- L'élimination des composants inutiles d'une configuration
- Les notifications en MBeans (suite)
- Le gestion centralisée de la configuration (netboot)
- Le binding pour faire cohabiter plusieurs instances de JBoss sur un serveur
