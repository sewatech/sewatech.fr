---
layout: course
permalink: formation-utilisation-linux.html
title: Utilisation du système Linux
title-short: Utilisation Linux
publication: 2008-02-21
code: as01
author: jean-philippe
duration: 3
price-level: 1
target: <p>Toute personne amenée à utiliser un ordinateur utilisant un système d'exploitation Linux (ou UNIX)&nbsp;&colon; RHEL, Fedora, Debian, Ubuntu, Solaris,... afin d'utiliser efficacement ce type de système et/ou afin de se préparer à l'administration du système.</p><p>Aucune connaissance particulière n'est requise, mais la connaissance préalable d'un système d'exploitation (Windows, MACOS, ...) est un plus.</p>
track: linux
logo: tux.png
keywords: 
- formation
- linux
intro: Ce cours décrit les principes de fonctionnement du système Linux et les commandes nécessaires à sa bonne utilisation.
---

Présentation générale du système Linux

Les principales versions du système Linux

Quelques commandes de base

Les systèmes de fichiers Linux

Les protections des fichiers

Gestion des processus

Les Shells

- Variables et méta-caractères
- Redirection des entrées/sorties
- Le mécanisme de "pipe"
- Facilités d'utilisation du Bash
- Introduction à la programmation de scripts Shell avec sh et bash

Description des éditeurs vi et emacs

Communication réseau avec ssh

Utilisation des CD, DVD et clefs USB

Paramétrage de l'environnement graphique utilisateur (Gnome, KDE)
