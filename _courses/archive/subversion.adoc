---
layout: course
permalink: formation-subversion.html
title: Subversion&nbsp;&colon; utilisation et administration
title-short: Formation subversion
publication: 2009-02-16
code: aj61
author: douglas
type: pratique
duration: 1
price-level: 9
target: Cette formation s'adresse aux développeurs et administrateurs d'environnement de développement souhaitant organiser leur code source autour de serveurs Sebversion.
track: administration
keywords: 
- formation
intro: <p>Successeur de CVS et Open Source, Subversion est aujourd'hui le logiciel de gestion de configuration le plus utilisé.</p><p>Cette formation vous permettra de comprendre les enjeux d'un tel outil. Elle vous apprendra à utiliser subversion et à gérer ses serveurs.</p>
---

Introduction

- Problématique du travail en équipe
- Organisation des tâches par domaine fonctionnel
- Accès concurrents aux ressources partagées
- Périmètres partageables
- Historique de Subversion et relation à CVS
- Tour d'horizon rapide des SCM (Source Code Management)

Architecture de Subversion

- Serveurs Subversion
- Protocoles d'accès aux serveurs
- Concept de dépôt (repository)
- Structuration d'un dépôt
- Clients de subversion et critères de choix

Concepts élémentaires de Subversion

- Définition d'un dépôt
- Définition d'un changes et et d'une révision
- Gestion des répertoires et des fichiers
- URL de chaque ressource
- Gestion des méta-données
- Organisation en branches et balises (tags)

Utilisation quotidienne

- Importation de contenu existant
- Récupération du contenu d'un repository
- Propagation d'une modification
- Synchronisations et résolution des conflits (merges)
- Retrouver l'auteur d'une modification

Administration des dépôts

- Création d'un dépôt
- Organisation d'un dépôt
- Utiliser les outils d'administration et de monitoring
- Export et import d'un dépôt
- Contrôle du contenu d'un dépôt
- Réparation d'un dépôt
- Sauvegardes à froid et à chaud, automatisées
- Scripting des événements (hook scripts)
- Envoi de mails lors des validation (commit)

Administration des serveurs

- SVNServe et d'Apache mod_dav_svn&nbsp;: présentation et critères de choix
- Configuration d'un Apache mod_dav_svn
- Configuration d'un SVNServe
- Sécurisation des échanges avec SSL
