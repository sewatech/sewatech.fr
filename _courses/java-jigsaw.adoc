---
title: Système de modules de Java
title-short: Formation jigsaw
publication: 2017-09-21

duration: 2
price-level: 2
importance: 2
track: java
logo: jigsaw.svg
prerequisites:
- Connaissance pratique de java
redirects: 
- formation-jigsaw.html
keywords: 
- java
- formation
introduction: 
- En 2009, la mort du classpath a été annoncée. 
  Il devait être remplacé par un système modulaire et tous nos problèmes de dépendance et de sécurité devaient se résoudre d'eux-mêmes. 
  Le projet Jigsaw est finalement intégré au JDK 9, en septembre 2017.
- La modularité se retrouve à la fois dans le JDK et nos applications. 
  Dans cette formation, nous expliquons comment la modularité se retrouve dans le JDK et nos applications. 
  Nous voyons quel problèmes ça résout et quels nouveaux problème ça pose.
description: |
  Formation pratique, en intra sur site ou à distance, sur la modularité du JDK et des applications avec le JPMS, alias Jigsaw.
---

Introduction

- Classpath et classloader
- Risque de classes hybrides
- Défauts de sécurité
- Problème d'obésité

Modularité du JDK

- De rt.jar aux modules
- Modules standards et module de base
- Modules dépréciés
- Modules non-standards
- Classes et packages supprimés
- Compilation, packaging et exécution

Modularité des applications

- Dépendances entre modules
- Export de packages
- Application multi-modules

Encapsulation des modules

- Évolution de la visibilité public
- Règles de répartition de packages
- Export de packages&nbsp;: globaux et limités
- Deep reflection
- Ouverture de package et de module

Dépendances entre modules

- Dépendances transitives
- Dépendances statiques
- Modules automatiques
- Mode mixte&nbsp;: module path / class path

Migration d'applications

- Option d'accès illégal
- Ajout de modules racine
- Ajout de dépendances
- Évaluation préalable des dépendances

Native

- Construction d'image personnalisée
- Compilation native
