---
title: Développer avec les lambdas et streams
title-short: Formation Java Lambda
publication: 2016-04-27

duration: 2
price-level: 2
importance: 2
track: java
logo: lambda_uncial.svg
prerequisites:
- Pratique courante du langage Java
keywords: 
- formation
- java
- lambda
- stream
introduction: 
- La version 8 de Java a apporté pas mal de modifications dont une nouvelle syntaxe&nbsp;&colon; les expressions lambda. 
  Ces dernières ont un impact énorme sur les APIs et sur notre façon d'écrire du code.
- Dans cette formation, vous apprendrez à utiliser cette nouvelle syntaxe. 
  Vous verrez comment l'utiliser pour exploiter les nouvelles APIs, en particulier celle des collections.
description: |
  Formation pratique, en intra sur site ou à distance, sur les expressions lambda en Java et les API de stream
---

Introduction

- Classe anonyme, fonction et lambda&nbsp;: question de lisibilité
- Impact sur les collections&nbsp;: du pattern d'itération à map/reduce

Expression lambda

- Présentation de la nouvelle notation '\->'
- Présentation des différentes formes de lambda
- Compatibilité avec les interfaces (fonctionnelles) existantes
- L'inférence de type dans les lambda
- La notation par method reference
- Lambda et variable final

Interfaces fonctionnelles

- L'objectif de rétro-compatibilité
- Définir une interface fonctionnelle
- L'annotation @FunctionalInterface
- Les nouvelles interfaces fonctionnelles&nbsp;: Function, Predicate,...
- Les méthodes default

Collections et Streams

- Les changements dans l'API de collection
- Les nouveaux patterns pour Collection et Map
- Passage de Collection à Stream
- Création de streams (types primitifs, String,...)
- Le pattern filter, map, collect
- Collectors standards et personnalisés
- Optional, à la place de null
