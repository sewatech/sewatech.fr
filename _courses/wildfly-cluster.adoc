---
title: JBoss / Wildfly en cluster
title-short: Formation Wildfly Cluster
publication: 2013-02-01
modification: 2021-12-01
duration: 3
price-level: 3
importance: 3
track: administration
logo: wildfly.svg
redirects: 
- formation-jboss-as7-cluster.html
author: david
teachers: 
- alexis
target: |
  Administrateurs et intégrateurs de serveurs d'applications Java, développeurs et architectes
prerequisites:
- Connaissance pratique de Wildfly ou JBoss EAP
practices: 
- A chaque étape, les stagiaires mettent en œuvre les techniques présentées, en déployant des applications Java EE et en intervenant dans les différents réglages de Wildfly. 
- Les exercices pratiques sont réalisés avec Wildfly ou JBoss EAP sous Windows, Linux ou MacOS.
keywords: 
- formation
- jboss
- clustering
introduction: 
- Wildfly est le leader des serveurs applicatifs Java EE libres et de plus en plus d'entreprises le choisissent au détriment de ses concurrents propriétaires.
- Ce  cours  avancé  s'adresse à tous ceux qui souhaitent mettre en place un environnement Wildfly ou JBoss EAP en cluster. 
  Les différents types de réplication et de répartition de charge sont abordés, sur les aspects HTTP, EJB et JMS. 
  A chaque étape, un atelier vous permet de mettre la théorie en pratique. 
  A la fin du cours, vous aurez les armes pour déployer des applications Java EE avec un haut niveau de disponibilité et une bonne scalabilité.
description: |
  Apprenez comment déployer un cluster de serveurs WildFly ou JBoss EAP avec cette formation de 3 jours (intra sur site ou à distance)
---
:leveloffset: +1

== Introduction

Concepts du clustering

- Quelques définitions&nbsp;: scalabilté, membership, farming, failover, ...
- Typologie des clusters
- Répartition de charge contre haute-disponibilité

Fonctionnalités de clustering de Wildfly

- Les sous-systèmes impliqués dans le clustering
- Les mécanismes de répartition
- La réplication des objets à état
- Clustering vertical contre clustering horizontal
- Les configurations prédéfinies dans Wildfly
- L'administration en domaine

Communication entre nœuds avec JGroups

- Rappels réseau&nbsp;: IP, TCP / UDP, multicast / unicast
- Les canaux et piles de protocoles
- Les protocoles présents dans les configurations par défaut de JBoss
- Les principaux paramètres de configuration

== Répartition HTTP

Apache en distributeur de charge

- Comment éviter le Single Point of Failure&nbsp;?
- Choix entre une répartition HTTP et un distributeur matériel
- Les techniques de répartition Apache&nbsp;: mod_jk, mod_proxy, mod_cluster

La solution JBoss&nbsp;: mod_cluster

- Les différents modules Apache
- La configuration dans Wildfly
- La configuration coté Apache
- Le calcul de charge des nœuds
- La gestion via JBoss CLI

== Réplication de sessions HTTP

Cache distribué avec Infinispan

- Les types de distribution et de réplication supportés par Infinispan
- La notion de cache-container
- Les options de synchronisation&nbsp;: concurrence et isolation
- Adaptation de la pile JGroups en fonction du type de réplication 
- Monitoring d'Infinispan

Réplication de session HTTP

- Réplication ou distribution&nbsp;?
- Les Configurations par défaut
- Les descripteurs de déploiement Web

== Clustering d'EJBs

EJBs session

- Rappels sur la configuration des pools
- Les algorithme de répartition de charge
- EJBs distants / EJBs locaux
- Répartition de charge des EJB stateless
- Répartition de charge et synchronisation des EJB stateful
- Configuration par défaut d'Infinispan

Entités JPA

- Infinispan et l'invalidation
- Le cache de second-niveau d'Hibernate
- Configuration par défaut de JBoss
- Les annotations spécifiques Hibernate

== Clustering JMS

Fonctionnalités ActiveMQ Artemis

- Groupe de serveur
- Découverte automatique
- Répartition de charge
- Répartition des connexions clientes
- Redistribution de messages

Clustering ActiveMQ Artemis

- Configuration prédéfinies Jboss
- Mise en place de la redistribution
- Usine à connexions HA
- Mise en place de serveurs de backup
- Clients JMS d'un cluster
