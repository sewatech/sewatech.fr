---
title: Sortie de LogWEx en version beta
publication: 2010-09-02
---

La première version beta de https://github.com/hasalex/logwex[LogWex] est disponible. 
Testez la et remontez nous les https://github.com/hasalex/logwex/issues[anomalies]...