---
title: Nouveau podcast Java
publication: 2011-05-11
---

L'équipe du http://www.lyonjug.org/[LyonJUG] a démarré *Cast-IT*, leur nouveau https://castit.sewatech.info/podcast.xml[podcast]. 
Les membres du JUG et leurs invités y discutent de Java, au sens large du terme : 
librairies, techniques de développement, communauté, langages alternatifs,...