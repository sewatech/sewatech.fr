---
title: Interview d'Alexis Hassler
publication: 2014-08-13
---

Arun Gupta, Director of Developer Advocacy chez Red Hat, a publié une https://blog.arungupta.me/2014/08/wildfly-administration-course-alexis-hassler/[interview d'Alexis Hassler] sur son blog, au sujet de notre link:/formation-wildfly.html[formation WildFly].