---
title: Vidéos du DevFest Nantes 2016
publication: 2016-11-21
---

Les vidéos de DevFest Nantes 2016 sont en ligne.

* https://youtu.be/HienTwtmjmk[Jigsaw est prêt à tuer le classpath Java], par Alexis Hassler
