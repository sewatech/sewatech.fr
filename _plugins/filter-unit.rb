# _plugins/shuffle.rb
module Jekyll
    module UnitFilter
      def unit(value, unit='', short_unit=nil)
        width = unit.length + 1 if unit != ''
        unit = unit + 's' if unit != '' && value && value > 1
        short_unit = short_unit ? short_unit : unit
        value ? "#{value}&nbsp;<span class=\"d-sm-inline-block d-none text-start\" style=\"width: #{width}ch\">#{unit}</span><span class=\"d-sm-none d-inline\">#{short_unit}</span>" : ""
      end
    end
  end
  
  Liquid::Template.register_filter(Jekyll::UnitFilter)
  