Jekyll::Hooks.register :site, :post_read do |site|
    active_courses = site.collections['courses'].docs
        .select { |page| !page.data['importance'].nil? }

    all_keywords = site.collections['articles'].docs
        .flat_map { |article| article.data['keywords'] }
        .uniq
        .map { |keyword| { 'key' => keyword } }
        .each do |keyword|
            keyword['courses'] = active_courses
                .select { |course| !course.data['keywords'].nil? }
                .select { |course| course.data['keywords'].include?(keyword['key']) }
        end

    site.data['keywords'] = all_keywords
end
