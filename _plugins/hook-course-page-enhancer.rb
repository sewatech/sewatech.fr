Jekyll::Hooks.register :site, :post_read do |site|
    site.data['formation']['tracks']
        .each { |track| track['courses'] = [] }

    formation_pages = site.collections['courses'].docs
        .each do |page|
            if (page.data['importance'].nil?)
                page.data['archive'] = true
                page.data['importance'] = 9
                page.data['price'] = nil
            else
                page.data['archive'] = false
                importance = page.data['importance']
                if (importance.to_s.start_with?('importance'))
                    page.data['importance'] = importance[10..-1].to_i
                end
            end

            site.collections['teachers'].docs
                .select { |author| author['code'] == page.data['author'] }
                .each { |author| page.data['author_full'] = author }
            site.data['formation']['tracks']
                .select { |track| track['id'] == page.data['track'] }
                .each do |track| 
                    page.data['track_full'] = track
                    page.data['logo'] = page.data['logo'] || track['logo']
                    track['courses'].push page
                end
            site.data['formation']['prices']
                .select { |price| price['level'] == page.data['price-level'] }
                .each do |price| 
                    begin
                        page.data['price'] = 
                            { 
                                "intra" => price['intra'] * page.data['duration'], 
                                "inter" => price['inter'] * page.data['duration']
                            } 
                    rescue 
                        page.data['price'] = nil
                    end
                end
            if (page.data['logo'].nil?)
                site.data['formation']['tracks']
                    .select { |track| track['id'] == page.data['track'] }
                    .each { |track| page.data['logo'] = track['logo'] }
            end
        end

    site.data['formation']['tracks']
        .each do |track| 
            track['courses'] = track['courses'].sort_by { |course| course['importance'] * rand(3..10) }
            track['first_courses'] = track['courses'][0..4]
        end

    site.data['courses'] = formation_pages
    site.data['first_courses'] = formation_pages
                .select { |course| !course['archive'] }
                .sort_by { |course| course['importance'] * rand(3..10) }[0..9]
end
