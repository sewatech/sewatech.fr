Jekyll::Hooks.register :pages, :pre_render do |page, payload|
    if page.class != Jekyll::RedirectPage
        page.data['seo'] = generate_seo(page)
    end
end
Jekyll::Hooks.register :documents, :pre_render do |document, payload|
    if document.class != Jekyll::RedirectPage
        document.data['seo'] = generate_seo(document)
    end
end


def generate_seo(page)
    seo = page.data['seo'] || Hash::new
    seo['@context'] = 'https://schema.org/'
    seo['name'] = page.data['title'] if page.data['title']
    seo['headline'] = page.data['title-short'] if page.data['title-short']
    seo['description'] = page.data['description'] if page.data['description']
    seo['datePublished'] = page.data['publication'] if page.data['publication']
    seo['dateModified'] = page.data['modification'] if page.data['modification']
    seo['keywords'] = page.data['keywords'].join(' ') if page.data['keywords']
    seo['image'] = page.site.config['url'] + '/images/formation/' + page.data['logo'] if page.data['logo']

    if (seo['@type'] == 'Course')
        provider =  Hash::new
        provider['@type'] = 'Organisation'
        provider['name'] = 'Sewatech'
        seo['provider'] = provider

        seo['coursePrerequisites'] = page.data['prerequisites'].join(', ') if page.data['prerequisites']

        courseInstance =  Hash::new
        courseInstance['@type'] = 'CourseInstance'
        courseInstance['courseMode'] = ['onsite', 'online']
        courseInstance['courseWorkload'] = "P#{page.data['duration']}D" if page.data['duration']
        courseOffer =  Hash::new
        courseOffer['@type'] = 'Offer'
        courseOffer['price'] = page.data['price']['intra'] if page.data['price']
        courseOffer['currency'] = 'EUR' if page.data['price']
        courseInstance['offer'] = courseOffer
        seo['hasCourseInstance'] = courseInstance
    end

    author = page.data['author_full']
    if (author)
        seo['author'] = Hash::new
        seo['author']['@type'] = author['type']
        seo['author']['name'] = author['title']
    end
    return seo
end