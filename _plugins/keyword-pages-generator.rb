module Jekyll

  class KeywordPageGenerator < Generator
    def generate(site)
      site.data['formation']['keywords'].each do |keyword|
        site.pages << KeywordPage.new(site, keyword)
      end
    end
  end

  class KeywordPage < Page
    def initialize(site, keyword)
      @site = site
      @base = site.source
      @name = "keyword-#{keyword['id']}.html"

      Jekyll.logger.debug "Generating page:", "#{keyword} => #{@name}"
      self.process(@name)
      self.read_yaml(File.join(@base, '_layouts'), 'keyword.html')
      self.data['keyword'] = keyword
      self.data['id'] = keyword['id']
      self.data['words'] = keyword['words']
      self.data['description'] = keyword['description']
      self.data['redirects'] = ["+--#{keyword['id']}-+.html"]
    end
  end

end
