module Jekyll

  class TrackPageGenerator < Generator
      def generate(site)
        site.data['formation']['tracks']
            .each do |track|
              site.pages << TrackPage.new(site, track)
            end
      end
    end
  
  class TrackPage < Page
      def initialize(site, track)
        @site = site
        @base = site.source
        @name = "formations-#{track['id']}.html"
        track['url'] = @name
  
        self.process(@name)
        self.read_yaml(File.join(@base, '_layouts'), 'track.html')
        self.data['redirects'] = ["-formations-#{track['id']}-.html"]
        self.data['track'] = track
        self.data['title'] = 'Formations ' + track['name']
        self.data['title-short'] = 'Formations ' + track['name']
        self.data['collection'] = 'courses'
        self.data['intro'] = track['description']
        self.data['archive'] = track['archive']
        self.data['seo'] = Hash::new
        self.data['seo']['@type'] = 'WebPage'
      end
  end

end